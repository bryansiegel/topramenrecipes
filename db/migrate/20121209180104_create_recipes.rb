class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :title
      t.text :recipe
      t.string :estimated_time_to_make

      t.timestamps
    end
  end
end
