class FixRecipeEstimatedTimeToCookColumn < ActiveRecord::Migration
 def change
 	change_column :recipes, :estimated_time_to_make, :string
 end
end