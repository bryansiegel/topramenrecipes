class Recipe < ActiveRecord::Base
  attr_accessible :estimated_time_to_make, :recipe, :title

validates_presence_of :title, :recipe, :estimated_time_to_make
validates_uniqueness_of :title, :recipe
end
