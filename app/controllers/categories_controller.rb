class CategoriesController < ApplicationController
  def index
    @category = Category.all
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(params[:category])
    if @category.save redirect_to @category, :notice => "Category created"
    else
      render :action => "new"
    end
  end

  def update
    @category = Category.find(params[:id])
    if @category.update_attributes(params[:category]) { redirect_to @category, :notice => "Category Updated"}
    else
      redirect_to :action => "new"
    end
  end

  def show
    @category = Category.find(params[:id])
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy
  end
end
